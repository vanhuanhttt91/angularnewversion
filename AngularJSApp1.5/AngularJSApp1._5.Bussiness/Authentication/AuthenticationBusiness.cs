﻿using AngularJSApp1._5.Data;
using AngularJSApp1._5.Repo.EntityDto;
using Serilog;
using ServiceStack;
using ServiceStack.Auth;
using System;

namespace AngularJSApp1._5.Bussiness.Authentication
{
    public class AuthenticationBusiness : IAuthenticationBusiness
    {
        private IAuthRepository _authRepository;

        public AuthenticationBusiness(IAuthRepository authRepository)
        {
            _authRepository = authRepository;
        }

        public ResultItem CreateUser(User user)
        {
            try
            {
                var userthen = user.ConvertTo<CustomUserAuthEntity>();
                _authRepository.CreateUserAuth(userthen, userthen.PasswordHash);
                return new ResultItem
                {
                    IsError = false,
                    Message = "Register user Success!"
                };
            }
            catch (Exception ex)
            {
                Log.Logger.Error("create user error with authentication" + ex.Message);
                return new ResultItem
                {
                    IsError = true,
                    Message = ex.Message
                };
            }

        }

        public ResultItem UpdateUser(User user)
        {
            throw new NotImplementedException();
        }
    }
}
