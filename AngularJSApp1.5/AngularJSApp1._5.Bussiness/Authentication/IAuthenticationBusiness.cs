﻿using AngularJSApp1._5.Data;
using AngularJSApp1._5.Repo.EntityDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularJSApp1._5.Bussiness.Authentication
{
    public interface IAuthenticationBusiness
    {
        ResultItem CreateUser(User user);

        ResultItem UpdateUser(User user);

    }
}
