﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngularJSApp1._5.Data;
using AngularJSApp1._5.Repo.EntityDto;
using AngularJSApp1._5.Repo.Repository;
using AngularJSApp1._5.Repo.UnitOfWork;
using Serilog;
using ServiceStack;

namespace AngularJSApp1._5.Bussiness
{
    public class CategoriesBusiness : BaseBusiness, ICategoriesBusiness
    {
        private readonly IRepository<CategoriesEntity> _categoriesRepo;
        private readonly IUnitOfWork _unitOfWork; 
        public CategoriesBusiness(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _categoriesRepo = unitOfWork.GetGenericRepository<CategoriesEntity>();
            _unitOfWork = unitOfWork;
        }

        public async Task<ResultItem> DeleteAsync(int categoriesId)
        {
            try
            {
                var query = _categoriesRepo.CreateQuery();
                await _categoriesRepo.DeleteAsync(categoriesId);
                _unitOfWork.Commit();
                return new ResultItem() { IsError = false, Message = "delete categories successfully!" };
            }
            catch(Exception ex)
            {
                Log.Logger.Error(String.Format("Bussiness Name: {0}; method Name: {1}; Message Error: {2}", nameof(CategoriesBusiness), "DeleteAsync", ex.Message));
                return new ResultItem() { IsError = true, Message = "delete categories fail!" };
            }
        }

        public async Task<ResultItem> EditAsync(Categories categories)
        {
            try
            {
                var query = _categoriesRepo.CreateQuery();
                var categoriesEntity = categories.ConvertTo<CategoriesEntity>();
                categoriesEntity.EditDate = DateTime.UtcNow;

                await _categoriesRepo.UpdateAsync(categoriesEntity, fieldChanged => new { fieldChanged.CategoriesName, fieldChanged.EditDate },
                    condition => condition.CategoriesId == categories.CategoriesId);
                _unitOfWork.Commit();
                return new ResultItem() { IsError = false, Message = "edit categories successfully!" };
            }
            catch(Exception ex)
            {
                Log.Logger.Error(String.Format("Bussiness Name: {0}; method Name: {1}; Message Error: {2}", nameof(CategoriesBusiness), "EditAsync", ex.Message));
                return new ResultItem() { IsError = true, Message = "edit categories successfully!" };
            }
        }

        public async Task<List<Categories>> GetAllAsync()
        {
            var categories = await _categoriesRepo.GetAllAsync();
            return categories.ConvertTo<List<Categories>>();
        }

        public async Task<DataResult<List<Categories>>> SearchCategoriesAsync(CategoriesSearch request)
        {
            try
            {
                var query = _categoriesRepo.CreateQuery();
                var orderBy = new string[] { };
                var orderByDesc = new string[] { };
                query = query.LeftJoin<PostEntity>();

                if (!string.IsNullOrWhiteSpace(request.CategoriesName))
                {
                    query = query.Where(x => x.CategoriesName.Contains(request.CategoriesName));
                }
                if (request.CategoriesId != 0)
                {
                    query = query.Where(x => x.CategoriesId.CompareTo(request.CategoriesId) > 1);
                }
                if (!string.IsNullOrEmpty(request.CreateDate.ToString()))
                {
                    query = query.Where(x => x.CreateDate.Value.Day == request.CreateDate.Value.Day
                                        && x.CreateDate.Value.Month == request.CreateDate.Value.Month
                                        && x.CreateDate.Value.Year == request.CreateDate.Value.Year);
                }
                if (!string.IsNullOrEmpty(request.EditDate.ToString()))
                {
                    query = query.Where(x => x.EditDate.Value.Day == request.EditDate.Value.Day
                                        && x.EditDate.Value.Month == request.EditDate.Value.Month
                                        && x.EditDate.Value.Year == request.EditDate.Value.Year);
                }
                if (request.Sort?.Any() != null && !request.Sort.IsEmpty())
                {
                    orderBy = request.Sort.Where(x => x.Dir.ToLower().Equals("asc")).Select(s => s.Field).ToArray();
                    orderByDesc = request.Sort.Where(x => x.Dir.ToLower().Equals("desc")).Select(s => s.Field).ToArray();
                }

                var categories = await _categoriesRepo.GetAsync<CategoriesEntity>(query, request.Skip, request.Take, orderby: orderBy, orderbydesc: orderByDesc);
                var total = await _categoriesRepo.CountAsync(query);
                return new DataResult<List<Categories>>
                {
                    Data = categories.ConvertTo<List<Categories>>(),
                    Total = total
                };
            }
            catch (Exception ex)
            {
                Log.Logger.Error(String.Format("Bussiness Name: {0}; method Name: {1}; Message Error: {2}", nameof(CategoriesBusiness), "SearchCategoriesAsync", ex.Message));
                return new DataResult<List<Categories>>();
            }
        }

        public async Task<ResultItem> InsertAsync(Categories categories)
        {
            try
            {
                var query = _categoriesRepo.CreateQuery();
                var categoriesEntity = categories.ConvertTo<CategoriesEntity>();
                categoriesEntity.CreateDate = DateTime.Now;
                await _categoriesRepo.InsertAsync(categoriesEntity);
                _unitOfWork.Commit();
                return new ResultItem() { IsError = false, Message = "add categories successfully!" };
            }
            catch(Exception ex)
            {
                Log.Logger.Error(String.Format("Bussiness Name: {0}; method Name: {1}; Message Error: {2}", nameof(CategoriesBusiness), "InsertAsync", ex.Message));
                return new ResultItem() { IsError = true, Message = "add categories fail!" };
            }
        }

    }
}
