﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AngularJSApp1._5.Data;

namespace AngularJSApp1._5.Bussiness
{
    public interface ICategoriesBusiness
    {
        Task<ResultItem> InsertAsync(Categories categories);
        Task<ResultItem> EditAsync(Categories categories);
        Task<ResultItem> DeleteAsync(int categoriesId);
        Task<DataResult<List<Categories>>> SearchCategoriesAsync(CategoriesSearch request);
        Task<List<Categories>> GetAllAsync();

    }
}
