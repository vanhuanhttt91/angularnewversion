﻿using AngularJSApp1._5.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AngularJSApp1._5.Bussiness
{
    public interface IPostBusiness
    {
        Task<ResultItem> InsertAsync(Post post);
        Task<ResultItem> DeleteAsync(int postId);
        Task<ResultItem> EditAsync(Post post);
        Task<List<Post>> GetAllAsync();
        Task<DataResult<List<Post>>> SearchPostsAsync(Post request);
    }
}
