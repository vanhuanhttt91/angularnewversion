﻿using AngularJSApp1._5.Repo.EntityDto;
using AngularJSApp1._5.Repo.UnitOfWork;

namespace AngularJSApp1._5.Bussiness
{
    public class InitDatabase : BaseBusiness, IInitDataBase
    {
         
            public InitDatabase(IUnitOfWork unitOfWork) : base(unitOfWork)
            {

            }

        public void Run()
        {
            var postRepo = UnitOfWork.GetGenericRepository<PostEntity>();
            var categoryRepo = UnitOfWork.GetGenericRepository<CategoriesEntity>();
            categoryRepo.CreateTableIfNotExists();
            postRepo.CreateTableIfNotExists();
            UnitOfWork.Commit();
        }
    }
    
}
