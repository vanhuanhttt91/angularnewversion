﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngularJSApp1._5.Data;
using AngularJSApp1._5.Repo.EntityDto;
using AngularJSApp1._5.Repo.Repository;
using AngularJSApp1._5.Repo.UnitOfWork;
using Serilog;
using ServiceStack;

namespace AngularJSApp1._5.Bussiness
{
    public class PostBusiness : BaseBusiness, IPostBusiness
    {
        private readonly IRepository<PostEntity> _repoPost;
        private readonly IUnitOfWork _unitOfWork;

        public PostBusiness(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
            _repoPost = unitOfWork.GetGenericRepository<PostEntity>();
            _unitOfWork = unitOfWork;
        }
        public async Task<ResultItem> DeleteAsync(int postId)
        {
            try
            {
                var query = _repoPost.CreateQuery();
                await _repoPost.DeleteAsync(postId);
                _unitOfWork.Commit();

                return new ResultItem() { IsError = false, Message = "delete Post successfully!" };
            }
            catch (Exception ex)
            {
                Log.Logger.Error(String.Format("Bussiness Name: {0}; method Name: {1}; Message Error: {2}", nameof(PostBusiness), "DeleteAsync", ex.Message));
                return new ResultItem() { IsError = true, Message = "delete Post fail!" };

            }
        }
        public async Task<ResultItem> EditAsync(Post post)
        {
            try
            {
                var query = _repoPost.CreateQuery();
                var postEntity = post.ConvertTo<PostEntity>();
                postEntity.EditDate = DateTime.UtcNow;
                await _repoPost.UpdateAsync(postEntity,
                    field => new { field.CategoriesId, field.Content, field.Slug, field.EditDate },
                condition => condition.PostId == post.PostId);
                _unitOfWork.Commit();

                return new ResultItem() { IsError = false, Message = "edit Post successfully!" };
            }
            catch (Exception ex)
            {
                Log.Logger.Error(String.Format("Bussiness Name: {0}; method Name: {1}; Message Error: {2}", nameof(PostBusiness), "EditAsync", ex.Message));
                return new ResultItem() { IsError = true, Message = "edit Post fail!" };

            }
        }
        public async Task<ResultItem> InsertAsync(Post post)
        {
            try
            {
                var query = _repoPost.CreateQuery();
                var postEntity = post.ConvertTo<PostEntity>();
                postEntity.CreateDate = DateTime.UtcNow;
                await _repoPost.InsertAsync(postEntity);
                _unitOfWork.Commit();

                return new ResultItem() { IsError = false, Message = "add new Post successfully!" };
            }
            catch (Exception ex)
            {
                Log.Logger.Error(String.Format("Bussiness Name: {0}; method Name: {1}; Message Error: {2}", nameof(PostBusiness), "InsertAsync", ex.Message));
                return new ResultItem() { IsError = true, Message = "add new Post fail!" };

            }
        }
        public async Task<List<Post>> GetAllAsync()
        {
            var result = await _repoPost.GetAllAsync();
            return result.ConvertTo<List<Post>>();
        }
        public async Task<DataResult<List<Post>>> SearchPostsAsync(Post request)
        {
            try
            {
                var query = _repoPost.CreateQuery();
                var orderBy = new string[] { };
                var orderByDesc = new string[] { };
                query = query.Join<CategoriesEntity>();

                if (request.PostId != 0)
                {
                    query = query.Where(x => x.PostId.CompareTo(request.PostId) > 1);
                }
                if (!string.IsNullOrWhiteSpace(request.Slug))
                {
                    query = query.Where(x => x.Slug.Contains(request.Slug));
                }
                if (!string.IsNullOrWhiteSpace(request.Content))
                {
                    query = query.Where(x => x.Content.Contains(request.Content));
                }
                if (!string.IsNullOrEmpty(request.CreateDate.ToString()))
                {
                    query = query.Where(x => x.CreateDate.Value.Day == request.CreateDate.Value.Day
                                        && x.CreateDate.Value.Month == request.CreateDate.Value.Month
                                        && x.CreateDate.Value.Year == request.CreateDate.Value.Year);
                }
                if (!string.IsNullOrEmpty(request.EditDate.ToString()))
                {
                    query = query.Where(x => x.EditDate.Value.Day == request.EditDate.Value.Day
                                        && x.EditDate.Value.Month == request.EditDate.Value.Month
                                        && x.EditDate.Value.Year == request.EditDate.Value.Year);
                }
                if (request.Sort?.Any() != null && !request.Sort.IsEmpty())
                {
                    orderBy = request.Sort.Where(x => x.Dir.ToLower().Equals("asc")).Select(s => s.Field).ToArray();
                    orderByDesc = request.Sort.Where(x => x.Dir.ToLower().Equals("desc")).Select(s => s.Field).ToArray();
                }

                var post = await _repoPost.GetAsync<Post>(query, request.Skip, request.Take, orderby: orderBy, orderbydesc: orderByDesc);
                var total = await _repoPost.CountAsync(query);
                return new DataResult<List<Post>>
                {
                    Data = post,
                    Total = total
                };
            }
            catch (Exception ex)
            {
                Log.Logger.Error(String.Format("Bussiness Name: {0}; method Name: {1}; Message Error: {2}", nameof(PostBusiness), "SearchPostsAsync", ex.Message));
                return new DataResult<List<Post>>();
            }
        }
    }

}
