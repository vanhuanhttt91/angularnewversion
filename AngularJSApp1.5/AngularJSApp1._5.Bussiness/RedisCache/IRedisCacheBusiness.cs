﻿using AngularJSApp1._5.Data;
using System;
using System.Collections.Generic;

namespace AngularJSApp1._5.Bussiness.RedisCache
{
    public interface IRedisCacheBusiness
    {
        bool IsInCache(string key);
        DataResult<List<Categories>> Get(CategoriesSearch search);
        bool RemoveFromCache();
        void Set<Categories>(Categories categories);
        void Set<Categories>(Categories categories, TimeSpan timeout);
    }
}
