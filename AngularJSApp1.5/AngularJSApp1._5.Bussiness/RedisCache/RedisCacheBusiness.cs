﻿using AngularJSApp1._5.Data;
using Serilog;
using ServiceStack;
using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;

namespace AngularJSApp1._5.Bussiness.RedisCache
{
    public class RedisCacheBusiness : IRedisCacheBusiness
    {
        private IRedisClientsManager _repositoryRedisCache;
        private readonly string cacheKey = WebConfigurationManager.AppSettings["categoriesCache"].ToString();

        public RedisCacheBusiness(IRedisClientsManager redisCache)
        {
            _repositoryRedisCache = redisCache;
        }

        public DataResult<List<Categories>> Get(CategoriesSearch request)
        {
            var repoRedis = _repositoryRedisCache.GetClient();
            var categories = repoRedis.Get<List<Categories>>(cacheKey);
            if (categories.IsNullOrEmpty())
            {
                return new DataResult<List<Categories>>();
            }
            var orderByField = new List<string>();
            var orderByDescField = new List<string>();
            var total = categories.Count;
            IQueryable<Categories> query = categories.AsQueryable();

            if (!string.IsNullOrEmpty(request.CategoriesName))
            {
                query = query.Where(x => x.CategoriesName.ToLower().Equals(request.CategoriesName));
            }
            if (!string.IsNullOrWhiteSpace(request.CategoriesName))
            {
                query = query.Where(x => x.CategoriesName.Contains(request.CategoriesName));
            }
            if (request.CategoriesId != 0)
            {
                query = query.Where(x => x.CategoriesId.CompareTo(request.CategoriesId) > 1);
            }
            if (!string.IsNullOrEmpty(request.CreateDate.ToString()))
            {
                query = query.Where(x => x.CreateDate.Value.Day == request.CreateDate.Value.Day
                                    && x.CreateDate.Value.Month == request.CreateDate.Value.Month
                                    && x.CreateDate.Value.Year == request.CreateDate.Value.Year);
            }
            if (!string.IsNullOrEmpty(request.EditDate.ToString()))
            {
                query = query.Where(x => x.EditDate.Value.Day == request.EditDate.Value.Day
                                    && x.EditDate.Value.Month == request.EditDate.Value.Month
                                    && x.EditDate.Value.Year == request.EditDate.Value.Year);
            }

            query = query.Skip(request.Skip).Take(request.Take);

            if (request.Sort?.Any() != null && !request.Sort.IsEmpty())
            {
                orderByField = request.Sort.Where(x => x.Dir.ToLower().Equals("asc")).Select(s => s.Field).ToList();
                orderByDescField = request.Sort.Where(x => x.Dir.ToLower().Equals("desc")).Select(s => s.Field).ToList();

                //switch (orderByField.First().ToString())
                //{
                //    case "CategoriesId":
                //        query = query.OrderBy(x => x.CategoriesId);
                //        break;
                //    case "CategoriesName":
                //        query = query.OrderBy(x => x.CategoriesName);
                //        break;
                //    case "CreateDate":
                //        query = query.OrderBy(x => x.CreateDate);
                //        break;
                //    case "EditDate":
                //        query = query.OrderBy(x => x.CreateDate);
                //        break;
                //}

                //switch (orderByDescField.First().ToString())
                //{
                //    case "CategoriesId":
                //        query = query.OrderByDescending(x => x.CategoriesId);
                //        break;
                //    case "CategoriesName":
                //        query = query.OrderByDescending(x => x.CategoriesName);
                //        break;
                //    case "CreateDate":
                //        query = query.OrderByDescending(x => x.CreateDate);
                //        break;
                //    case "EditDate":
                //        query = query.OrderByDescending(x => x.CreateDate);
                //        break;
                //}
            }

            return new DataResult<List<Categories>>
            {
                Total = total,
                Data = query.ToList()
            };
        }

        public bool IsInCache(string key)
        {
            throw new NotImplementedException();
        }

        public bool RemoveFromCache()
        {

            try
            {
                var repoRedis = _repositoryRedisCache.GetClient();
                return repoRedis.Remove(cacheKey);
            }
            catch (Exception ex)
            {
                Log.Logger.Error("error rediscache remove function");
                return false;
            }

        }

        public void Set<Categories>(Categories categories)
        {
            var _repoRedis = _repositoryRedisCache.GetClient();
            _repoRedis.Set<Categories>(cacheKey, categories);
        }

        public void Set<Categories>(Categories categories, TimeSpan timeout)
        {
            var _repoRedis = _repositoryRedisCache.GetClient();
            _repoRedis.Set<Categories>(cacheKey, categories, timeout);
        }
    }
}
