﻿using ServiceStack;
using System.Collections.Generic;
using System.Linq;

namespace AngularJSApp1._5.Data.AuthenRequest
{
    public static class AthenRequest
    {
        public static List<string> IpAddressWhiteList = new List<string>();

        public static bool IsValidIpAddress(string ipAddress)
        {
            if (IpAddressWhiteList?.Any() != true)
                return true;

            if (ipAddress.IsNullOrEmpty() || IpAddressWhiteList.All(x => x != ipAddress))
                return false;

            return true;
        }
    }
}
