﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace AngularJSApp1._5.Data
{
    public class BaseSearchRequest
    {
        [JsonProperty(PropertyName = "skip")]
        public int Skip { get; set; }

        [JsonProperty(PropertyName = "top")]
        public int Take { get; set; } = 10;

        [JsonProperty(PropertyName = "filter")]
        public string Filter { get; set; }

        [JsonProperty(PropertyName = "page")]
        public int Page { get; set; }

        [JsonProperty(PropertyName = "pageSize")]
        public int PageSize { get; set; }

        [JsonProperty(PropertyName = "sort")]
        public IList<KendoSort> Sort { get; set; }
    }
    public class KendoSort
    {
        [JsonProperty(PropertyName = "sort")]
        public string Field { get; set; }

        [JsonProperty(PropertyName = "dir")]
        public string Dir { get; set; }
    }
}
