﻿
using System.Collections.Generic;

namespace AngularJSApp1._5.Data
{
    public class Categories
    {
        public int CategoriesId { get; set; }

        public string CategoriesName { get; set; }
        public System.DateTime? CreateDate { get; set; }
        public System.DateTime? EditDate { get; set; }
    }

    public class CategoriesSearch: BaseSearchRequest
    {
        public int CategoriesId { get; set; }
        public string CategoriesName { get; set; }
        public System.DateTime? CreateDate { get; set; }
        public System.DateTime? EditDate { get; set; }
    }
}
