﻿using System.Collections.Generic;

namespace AngularJSApp1._5.Data
{
    public class DataResult<T>
    {
        public long Total { get; set; }
        public T Data { get; set; }
    }
}
