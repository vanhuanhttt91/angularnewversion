﻿
using System;
using ServiceStack;

namespace AngularJSApp1._5.Data
{
    public class Post : BaseSearchRequest
    {
        public Post()
        {
        }
        public int PostId { get; set; }
        public string Slug { get; set; }
        public string Content { get; set; }
        public int CategoriesId { get; set; }
        public string CategoriesName { get; set; }
        public Categories Category
        {
            get
            {
                return new Categories
                {
                    CategoriesId = this.CategoriesId,
                    CategoriesName = this.CategoriesName
                };
                }
        }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
    }
}
