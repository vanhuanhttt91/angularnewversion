﻿using System;

namespace AngularJSApp1._5.Data
{
    public class ResultItem
    {
        public string Message { get; set; }
        public Object Items { get; set; }
        public bool IsError { get; set; }
    }
}
