﻿
using System.Collections.Generic;

namespace AngularJSApp1._5.Data
{
    public class User
    {
        public int Id { get; set; }

        public string UserName { get; set; }
        public string Email { get; set; }

        public int Age { get; set; }

        public System.DateTime CreatedDate { get; set; }

        public System.DateTime ModifieldDate { get; set; }
        public string PasswordHash { get; set; }
        public virtual List<string> Permissions { get; set; }
    }
}
