﻿using ServiceStack.Auth;
using ServiceStack.DataAnnotations;

namespace AngularJSApp1._5.Repo.EntityDto
{
    [Alias("CustomUserAuthDetails")]
    public class CustomUserAuthDetailsEntity : UserAuthDetails
    {
    }
}
