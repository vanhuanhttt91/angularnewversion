﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularJSApp1._5.Repo.EntityDto
{

    public static class PermissionName
    {
        public static string Add = "Add";
        public static string Edit = "Edit";
        public static string Delete = "Delete";
    }

}
