﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.DataAnnotations;

namespace AngularJSApp1._5.Repo.EntityDto
{
    [Alias("Post")]
    public class PostEntity
    {
        [PrimaryKey]
        [AutoIncrement]
        public int PostId { get; set; }
        public string Slug { get; set; }

        public string Content { get; set; }

        [References(typeof(CategoriesEntity))]
        public int CategoriesId { get; set; }
        [Reference]
        public CategoriesEntity Categories { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
    }
}
