﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AngularJSApp1._5.Repo.Repository
{
    public interface IRepository<T> where T : class
    {
        SqlExpression<T> CreateQuery();

        Task<List<T>> GetAllAsync();

        Task<List<T>> GetAsync(SqlExpression<T> exp, int? skip = 0, int? take = int.MaxValue, string[] orderby = null, string[] orderbydesc = null);

        Task<List<TResult>> GetAsync<TResult>(SqlExpression<T> exp, int? skip = 0, int? take = int.MaxValue, string[] orderby = null, string[] orderbydesc = null);

        Task<T> GetByIdAsync(int id);

        Task InsertAsync(T o);

        Task UpdateAsync(T o, Expression<Func<T, object>> onlyFields = null, Expression<Func<T, bool>> where = null);

        Task<bool> SaveAsync(T o);

        Task DeleteAsync(int id);

        Task DeleteAsync(Expression<Func<T, bool>> exp);

        Task<long> CountAsync(SqlExpression<T> exp);

        void CreateTableIfNotExists();
    }
}
