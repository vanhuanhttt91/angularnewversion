﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AngularJSApp1._5.Repo.Repository
{

    public class Repository<T> : RepositoryBase, IRepository<T> where T : class
    {
        public Repository(IDbConnection connection) : base(connection)
        {
        }

        public virtual SqlExpression<T> CreateQuery()
        {
            var sqlExpression = Connection.From<T>();

            return sqlExpression;
        }

        public virtual async Task<List<T>> GetAllAsync()
        {
            return await Connection.SelectAsync<T>();
        }

        public virtual async Task<List<T>> GetAsync(SqlExpression<T> exp, int? skip = 0, int? take = int.MaxValue, string[] orderby = null, string[] orderbydesc = null)
        {

            return await GetAsync<T>(exp, skip, take, orderby, orderbydesc);
        }

        public virtual async Task<List<TResult>> GetAsync<TResult>(SqlExpression<T> exp, int? skip = 0, int? take = int.MaxValue, string[] orderby = null, string[] orderbydesc = null)
        {
            var query = exp.Skip(skip).Take(take);

            if (orderby?.Any() == true)
            {
                query = query.OrderByFields(orderby);
            }

            if (orderbydesc?.Any() == true)
            {
                query = query.OrderByFieldsDescending(orderbydesc);
            }

            return await Connection.SelectAsync<TResult>(query);
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            var o = await Connection.SingleByIdAsync<T>(id);

            return o;
        }

        public virtual async Task InsertAsync(T o)
        {
            await Connection.InsertAsync(o);
        }

        public virtual async Task UpdateAsync(T o, Expression<Func<T, object>> onlyFields = null, Expression<Func<T, bool>> where = null)
        {
            await Connection.UpdateOnlyAsync(o, onlyFields, where);
        }

        public virtual async Task<bool> SaveAsync(T o)
        {
            return await Connection.SaveAsync(o);
        }

        public virtual async Task DeleteAsync(int id)
        {
            await Connection.DeleteByIdAsync<T>(id);
        }

        public virtual async Task DeleteAsync(Expression<Func<T, bool>> exp)
        {
            await Connection.DeleteAsync(exp);
        }

        public virtual async Task<long> CountAsync(SqlExpression<T> exp)
        {
            return await Connection.CountAsync(exp);
        }

        public virtual void CreateTableIfNotExists()
        {
            Connection.CreateTableIfNotExists<T>();
        }
    }

    public class RepositoryBase
    {
        protected IDbConnection Connection;

        public RepositoryBase(IDbConnection connection)
        {
            Connection = connection;
        }
    }

}
