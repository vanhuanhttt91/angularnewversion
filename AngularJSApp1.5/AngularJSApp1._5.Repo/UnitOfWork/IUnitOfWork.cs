﻿using AngularJSApp1._5.Repo.Repository;
using System;

namespace AngularJSApp1._5.Repo.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<T> GetGenericRepository<T>() where T : class;

        TRepository GetRepository<TRepository>() where TRepository : class;

        void Commit();
        void Rollback();
    }
}
