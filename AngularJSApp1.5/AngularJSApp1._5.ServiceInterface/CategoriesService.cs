﻿using AngularJSApp1._5.Bussiness;
using AngularJSApp1._5.Bussiness.RedisCache;
using AngularJSApp1._5.Data;
using AngularJSApp1._5.ServiceModel;
using ServiceStack;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace AngularJSApp1._5.ServiceInterface
{
    [Authenticate]
    public class CategoriesService : Service
    {
        public ICategoriesBusiness _categoriesBusiness { get; set; }
        public IRedisCacheBusiness _redisCacheBussiness { get; set; }

        public async Task<List<Categories>> Get(GetAll request)
        {
            var result = await _categoriesBusiness.GetAllAsync().ConfigureAwait(false);
            return result.ConvertTo<List<Data.Categories>>();
        }

        [Authenticate]
        public async Task<DataResult<List<Categories>>> Post(SearchCategories request)
        {
            var categoriesCache = _redisCacheBussiness.Get(request);
            if (categoriesCache.Total == 0)
            {
                var categoriesAll = await _categoriesBusiness.GetAllAsync().ConfigureAwait(false);
                _redisCacheBussiness.Set(categoriesAll);
                return _redisCacheBussiness.Get(request);
            }

            //var result = await _categoriesBusiness.SearchCategoriesAsync(request).ConfigureAwait(false);
            return categoriesCache;
        }

        public async Task<ResultItem> Post(EditCategories request)
        {
            var result = await _categoriesBusiness.EditAsync(request.ConvertTo<Categories>()).ConfigureAwait(false);
            _redisCacheBussiness.RemoveFromCache();
            return result;
        }

        public async Task<ResultItem> Post(AddCategories request)
        {
            var result = await _categoriesBusiness.InsertAsync(request.ConvertTo<Categories>()).ConfigureAwait(false);
            _redisCacheBussiness.RemoveFromCache();
            return result;
        }

        public async Task<ResultItem> Post(DeleteCategories request)
        {
            var result = await _categoriesBusiness.DeleteAsync(request.CategoriesId).ConfigureAwait(false);
            _redisCacheBussiness.RemoveFromCache();
            return result;
        }
    }
}
