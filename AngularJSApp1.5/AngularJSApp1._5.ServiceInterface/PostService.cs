﻿using AngularJSApp1._5.Bussiness;
using AngularJSApp1._5.Data;
using AngularJSApp1._5.ServiceModel;
using Serilog;
using ServiceStack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularJSApp1._5.ServiceInterface
{
    [Authenticate]
    public class PostService : Service
    {
        public IPostBusiness _postBusiness { get; set; }

        public async Task<List<Post>> Get(PostModel request)
        {
            var result = await _postBusiness.GetAllAsync().ConfigureAwait(false);
            return result ;
        }
        public async Task<DataResult<List<Post>>> Post(Posts request)
        {
            var result = await _postBusiness.SearchPostsAsync(request).ConfigureAwait(false);
            return result;
        }
        public async Task<ResultItem> Post(AddPost request)
        {
            var result = await _postBusiness.InsertAsync(request).ConfigureAwait(false);
            return result; 
        }
        public async Task<ResultItem> Post(EditPost request)
        {
            var result = await _postBusiness.EditAsync(request).ConfigureAwait(false);
            return result;
        }
        public async Task<ResultItem> Post(DeletePost request)
        {
            var result = await _postBusiness.DeleteAsync(request.PostId).ConfigureAwait(false);
            return result;
        }
    }
}
