﻿using AngularJSApp1._5.Bussiness.Authentication;
using AngularJSApp1._5.Data;
using AngularJSApp1._5.ServiceModel;
using ServiceStack;

namespace AngularJSApp1._5.ServiceInterface
{
    class UserService : Service
    {
        public IAuthenticationBusiness _authenticationBusiness { get; set; }

        public ResultItem Post(RegisterModel request)
        {
           return _authenticationBusiness.CreateUser(request);
        }
    }
}
