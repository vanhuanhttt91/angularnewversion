﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack;
using AngularJSApp1._5.Data;

namespace AngularJSApp1._5.ServiceModel
{
 
    public class CategoriesModel : IReturn
    {

    }

    [Route("/api/getcategories", "GET")]
    public class GetAll : IReturn<List<Categories>>
    {

    }

    [Route("/api/editCategories", "POST")]
    public class EditCategories : Categories, IReturn<Categories>
    {
        
    }

    [Route("/api/addCategories", "POST")]
    public class AddCategories : Categories, IReturn<Categories>
    {

    }

    [Route("/api/deleteCategories", "POST")]
    public class DeleteCategories : IReturn
    {
        public int CategoriesId { get; set; }
    }
    [Route("/api/categories", "POST")]
    public class SearchCategories: CategoriesSearch , IReturn<List<Categories>>
    {

    }
}
