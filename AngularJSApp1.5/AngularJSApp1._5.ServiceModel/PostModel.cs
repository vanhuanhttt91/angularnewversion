﻿using AngularJSApp1._5.Data;
using ServiceStack;
using System.Collections.Generic;

namespace AngularJSApp1._5.ServiceModel
{
    [Route("/api/getposts", "GET")]
    public class PostModel : IReturn<List<Post>>
    {
    }

    [Route("/api/searchposts", "POST")]
    public class Posts : Post, IReturn<ResultItem>
    {

    }

    [Route("/api/addpost", "POST")]
    public class AddPost: Post, IReturn<ResultItem>
    {

    }

    [Route("/api/editpost", "POST")]
    public class EditPost : Post, IReturn<ResultItem>
    {

    }

    [Route("/api/deletepost", "POST")]
    public class DeletePost : Post, IReturn<ResultItem>
    {

    }
}
