﻿using System.IO;
using Funq;
using AngularJSApp1._5.ServiceInterface;
using ServiceStack;
using ServiceStack.Configuration;
using ServiceStack.Razor;
using ServiceStack.OrmLite;
using System.Web.Configuration;
using ServiceStack.Data;
using AngularJSApp1._5.Repo.UnitOfWork;
using AngularJSApp1._5.Bussiness;
using ServiceStack.Auth;
using AngularJSApp1._5.Repo.EntityDto;
using ServiceStack.Logging;
using ServiceStack.Logging.Serilog;
using Serilog;
using ServiceStack.Redis;
using System;
using AngularJSApp1._5.Bussiness.RedisCache;
using System.Collections.Generic;
using AngularJSApp1._5.Bussiness.Authentication;
using ServiceStack.Host;
using AngularJSApp1._5.Data.AuthenRequest;

namespace AngularJSApp1._5
{
    public class AppHost : AppHostBase
    {
        /// <summary>
        /// Base constructor requires a Name and Assembly where web service implementation is located
        /// </summary>
        public AppHost()
            : base("AngularJSApp1._5", typeof(CategoriesService).Assembly)
        {
            var customSettings = new FileInfo(@"~/appsettings.txt".MapHostAbsolutePath());
            AppSettings = customSettings.Exists
                ? (IAppSettings)new TextFileSettings(customSettings.FullName)
                : new AppSettings();
        }

        /// <summary>
        /// Application specific configuration
        /// This method should initialize any IoC resources utilized by your web service classes.
        /// </summary>
        public override void Configure(Container container)
        {
            //Config examples
            this.Plugins.Add(new PostmanFeature());            
            this.Plugins.Add(new CorsFeature(allowedHeaders: "Content-Type, Authorization"));
            OrmLiteConfig.DialectProvider = SqlServerDialect.Provider;
            var connectionString = WebConfigurationManager.ConnectionStrings["ConnStringDb1"].ToString();
            var redisConnection = new RedisEndpoint
            {
                Db = Int32.Parse(WebConfigurationManager.AppSettings["redis:db"]),
                Host = WebConfigurationManager.AppSettings["redis:host"],
                Port = Int32.Parse(WebConfigurationManager.AppSettings["redis:port"])
            };
            container.Register<IDbConnectionFactory>(new OrmLiteConnectionFactory(connectionString));
            container.Register<IRedisClientsManager>(c => new RedisManagerPool(redisConnection.ToString()));
            container.RegisterAutoWiredAs<UnitOfWork, IUnitOfWork>().ReusedWithin(ReuseScope.Request);
            container.RegisterAutoWiredAs<InitDatabase, IInitDataBase>().ReusedWithin(ReuseScope.Request);
            container.RegisterAutoWiredAs<CategoriesBusiness, ICategoriesBusiness>().ReusedWithin(ReuseScope.Request);
            container.RegisterAutoWiredAs<PostBusiness, IPostBusiness>().ReusedWithin(ReuseScope.Request);
            container.RegisterAutoWiredAs<RedisCacheBusiness, IRedisCacheBusiness>().ReusedWithin(ReuseScope.Request);
            container.RegisterAutoWiredAs<AuthenticationBusiness, IAuthenticationBusiness>().ReusedWithin(ReuseScope.Request);

            var initDb = container.Resolve<IInitDataBase>();
            initDb.Run();
            SettingAuthProvider(container);
            SettingSeriLog();

            SetConfig(new HostConfig
            {
                DebugMode = AppSettings.Get("DebugMode", false),
                AddRedirectParamsToQueryString = true
            });


            this.Plugins.Add(new RazorFormat());
        }

        private void SettingAuthProvider(Container container)
        {
            var authRepo = new OrmLiteAuthRepository<CustomUserAuthEntity, CustomUserAuthDetailsEntity>(container.Resolve<IDbConnectionFactory>());
            container.Register<IUserAuthRepository>(authRepo);
            authRepo.InitSchema();

            Plugins.Add(new AuthFeature(() => new AuthUserSession(), new IAuthProvider[]
            {
                new JwtAuthProvider(AppSettings)
                {
                    RequireSecureConnection = false,
                    IncludeJwtInConvertSessionToTokenResponse = true,
                    ValidateToken = (o, request) => true
                },
                new CredentialsAuthProvider(AppSettings)
            })
            {
                IncludeRegistrationService = true,  // Allow register user
                AllowGetAuthenticateRequests = request => true,    // Require get authenticate info by other GET method
                DeleteSessionCookiesOnLogout = true

            });


            // Create admin account
            if (authRepo.GetUserAuthByUserName("admin") == null)
            {
                authRepo.CreateUserAuth(new CustomUserAuthEntity
                {
                    UserName = "admin",
                    Roles = new List<string> { RoleNames.Admin },
                    DisplayName = "Admin",
                    Permissions = new List<string> { PermissionName.Add, PermissionName.Edit, PermissionName.Delete }
                }, "123");

                var userAuth = authRepo.GetUserAuthByUserName("admin");
                if (userAuth != null)
                {
                    authRepo.AssignRoles(userAuth, new [] { RoleNames.Admin }, new [] { PermissionName.Add, PermissionName.Edit, PermissionName.Delete });
                }
            }

            GlobalRequestFilters.Add((req, res, dto) =>
            {
                // Custom filter request 
                // Filter ip address
                if (AthenRequest.IsValidIpAddress(req.UserHostAddress)) return;
                res.StatusCode = 403;
                res.StatusDescription = "InvalidIpAddress";
                res.EndRequest();
            });
        }

        private void SettingSeriLog()
        {
            LogManager.LogFactory = new SerilogFactory();
            Log.Logger = new LoggerConfiguration()
              .ReadFrom.AppSettings()
              .WriteTo.LiterateConsole()
              .WriteTo.RollingFile(@"~\logs\log-{Date}.txt", retainedFileCountLimit: 10)
              .MinimumLevel.Verbose()
              .CreateLogger();

            Log.Logger.Information("Application Started");

        }
    }
}