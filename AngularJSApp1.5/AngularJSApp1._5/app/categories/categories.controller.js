﻿
/* global angular */
(function () {
    "use strict";

    angular
        .module('app.categories', ["kendo.directives"])
        .controller('categoriesCtrl', categoriesController);

    function categoriesController(categoriesServices) {

        var vm = this;
        let categories = {
            "CategoriesId": '',
            "CategoriesName": ''
        };
        vm.token = localStorage.getItem(userLoginKey) != null ? JSON.parse(localStorage.getItem(userLoginKey)).BearerToken: '';
       
        vm.mainSearch = {};

        vm.editCategories = function (item) {
            categories.CategoriesId = item.CategoriesId;
            categories.CategoriesName = item.CategoriesName;
            categoriesServices.editCategories(categories).then(function (data) {
                if (data.IsError) {
                    vm.showPopupFail(data.Message);
                } else {
                    vm.showPopupSuccess(data.Message);
                }
                vm.refreshGrid();
            }).catch(function (data) {
                vm.showPopupSuccess(data.Message);
            });
        };

        vm.deleteCategories = function (item) {

            categories.CategoriesId = item.CategoriesId;
            if (checkNullOrEmpty(categories.CategoriesId)) {
                vm.showPopupFail("Categories Id is unvalid");
                return;
            }
            categoriesServices.deleteCategories(categories).then(function (data) {
                if (data.IsError) {
                    vm.showPopupFail(data.Message);
                } else {
                    vm.showPopupSuccess(data.Message);
                }
                vm.refreshGrid();
            }).catch(function (data) {
                vm.showPopupFail(data.Message);
                vm.refreshGrid();
            });
        };

        vm.addCategories = function () {

            categories.CategoriesName = vm.categoriesNameAdd;
            if (checkNullOrEmpty(categories.CategoriesName)) {
                vm.showPopupFail("Categories Name is unvalid");
                return;
            }
            categoriesServices.addCategories(categories).then(function (data) {
                if (data.IsError) {
                    vm.showPopupFail(data.Message);
                } else {
                    vm.showPopupSuccess(data.Message);
                }
                vm.refreshGrid();
            }).catch(function (data) {
                vm.notifyFailOptions(data.Message);
            });
        };

        vm.refreshGrid = function () {
            $("#kgrid").data("kendoGrid").dataSource.read();
        };

        vm.mainGridOptions = {
            dataSource: {
                transport: {
                    read: {
                        url: envBaseUrl + '/categories',
                        type: 'POST',
                        headers: { Authorization: "Bearer " + vm.token } ,
                        contentType: 'application/json',
                        data: {}
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                pageSize: 5,
                serverPaging: true,
                serverSorting: true,
                schema: {
                    total: "Total",
                    data: function (response) {
                        console.log(response);
                        response.Data.forEach(function (item) {
                            item.CreateDate = ConvertJsonDateToDate(item.CreateDate);
                            item.EditDate = ConvertJsonDateToDate(item.EditDate);
                        });
                        return response.Data; 
                    },
                    model: {
                        id: "CategoriesId",
                        fields: {
                            CategoriesId: { type: "number", editable: false },
                            CategoriesName: { type: "string", validation: { required: true } },
                            CreateDate: { type: "string", editable: false },
                            EditDate: { type: "string", editable: false }
                        }
                    }
                }
            },
            selectable: true,
            sortable: true,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 15]

            },
            autoSync: true,
            editable: true,
            toolbar: [
                {
                    name: "create",
                    text: "Add New Categories",
                    template: '<a ng-click="winAddCategories.open()" class="k-button k-button-icontext k-grid-create">Add New Categories</a>'
                },
                {
                    name: "search",
                    template: '<input ng-model="vm.mainSearch" autocomplete="off" placeholder="Search filed..." title="Search..." class="k-input">'
                }
            ],
            databound: function () {
                this.expandrow(this.tbody.find("tr.k-master-row").first());
            },
            columns: [{
                field: "CategoriesId",
                title: "Categories Id",
                width: "120px"
            }, {
                field: "CategoriesName",
                title: "Categories Name",
                width: "120px"
            }, {
                field: "CreateDate",
                width: "120px",
                disable: "true"
            }, {
                field: "EditDate",
                width: "120px"
            },
            {
                command: [
                    { name: "edit", template: '<a ng-click="vm.editCategories(this.dataItem)" class="k-button k-button-icontext k-grid-edit" >Edit</a>' },
                    { name: "delete", template: '<a ng-click="vm.deleteCategories(this.dataItem)" class="k-button k-button-icontext k-grid-delete" >Delete</a>' }
                ]
            }
            ]
        };

        vm.notifySuccessOptions = {
            templates: [{
                type: "success",
                template: $("#notificationSuccess").html()
            }]
        };

        vm.notifyFailOptions = {
            templates: [{
                type: "error",
                template: $("#notificationFail").html()
            }]
        };

        vm.showPopupSuccess = function (message) {
            vm.notifySuccess.show({
                kValue: message
            }, "success");
        };

        vm.showPopupFail = function (message) {
            vm.notifyFail.show({
                kValue: message
            }, "error");
        };
    }
})();