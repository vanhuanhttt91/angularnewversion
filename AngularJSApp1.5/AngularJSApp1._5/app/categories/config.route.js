﻿(function () {
    'use strict';

    angular
        .module('app.categories')
        .run(appRun);
    //  appRun.$inject = ['routehelper'];
    /* inject */
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/categories',
                config: {
                    templateUrl: '/app/categories/categories.html',
                    controller: 'categoriesCtrl',
                    controllerAs: 'vm',
                    title: 'categories'

                }
            }
        ];
    }
})();