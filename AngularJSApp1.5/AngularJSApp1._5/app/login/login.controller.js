﻿/* global angular */
(function () {
    "use strict";
    angular.module("app.login", ["kendo.directives"])
        .controller("loginController", loginController);

    function loginController($scope, $rootScope, loginService) {

        $scope.LoginModel = {
            UserName: '',
            PassWord: ''
        };

        $scope.validate = function (event) {
            event.preventDefault();

            if ($scope.validator.validate()) {
                $scope.validationMessage = "Hooray! Your tickets has been booked!";
                $scope.validationClass = "valid";
            } else {
                $scope.validationMessage = "Tên đăng nhập hoặc mật khẩu sai!";
                $scope.validationClass = "invalid";
            }
        };

        $scope.login = function () {

            loginService.login($scope.LoginModel).then(function (data) {
                if (data.IsError) {
                    $scope.showPopupFail(data.Message);
                    //$scope.validationMessage = "Tên đăng nhập hoặc mật khẩu sai!";
                } else {
                    $scope.showPopupSuccess(data.Message);
                    $rootScope.$broadcast('closeLoginWindow', {});
                }
                
            });

            $scope.resetLoginForm();
        };

        $scope.cancel = function () {
            $rootScope.$broadcast('closeLoginWindow', {});
            $scope.resetLoginForm();
        };

        $scope.resetLoginForm = function () {
            $scope.validationClass = '';
        };

        $scope.notifySuccessOptions = {
            templates: [{
                type: "success",
                template: $("#notificationSuccess").html()
            }]
        };

        $scope.notifyFailOptions = {
            templates: [{
                type: "error",
                template: $("#notificationFail").html()
            }]
        };

        $scope.showPopupSuccess = function (message) {
            $scope.notifySuccess.show({
                kValue: message
            }, "success");
        };

        $scope.showPopupFail = function (message) {
            $scope.notifyFail.show({
                kValue: message
            }, "error");
        };
    }

})();
