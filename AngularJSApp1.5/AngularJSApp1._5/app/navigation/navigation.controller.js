﻿/* global angular */
(function () {
    "use strict";

    angular.module('app.navigation', [])
        .controller('navigationCtrl', ['$scope', '$location', '$rootScope', '$http',
            function ($scope, $location, $rootScope, $http) {


                $scope.$on('closeLoginWindow', function (evt, data) {
                    $scope.loginWindow.close();
                });

                $scope.IsRouteActive = function (routePath) {
                    return routePath === $location.path();

                };

              //  $scope.isActive =  localStorage.getItem(userLoginKey) === null ? false : true;


                $scope.logout = function () {
                    return $http({
                        method: 'POST',
                        data: {},
                        headers: { 'Content-Type': 'application/json' },
                        url: envBaseUrlAuthen + '/auth/logout'
                    }).then(getReponseComplete).catch(getReponseFail);

                    function getReponseComplete(response) {
                        $location.path("/");
                        localStorage.removeItem(userLoginKey);
                        // $scope.isActive = false;
                        return response.data;
                    }

                    function getReponseFail(message) {
                        exception.catcher('XHR Failed for addCategories')(message);
                        return message;
                    }
                };
            }
        ]);
})();
