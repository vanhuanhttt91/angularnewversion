﻿(function () {
    'use strict';

    angular
        .module('app.post')
        .run(appRun);

    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/post',
                config: {
                    templateUrl: '/app/post/post.html',
                    controller: 'postCtrl',
                    controllerAs: 'vm',
                    title: 'Post'
                }
            }];
    }

})();