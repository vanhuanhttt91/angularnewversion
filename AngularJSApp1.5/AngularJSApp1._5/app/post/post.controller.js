﻿/* global angular */
(function () {
    "use strict";

    angular
        .module('app.post', ["kendo.directives"])
        .controller('postCtrl', postController);

    function postController(categoriesServices, postService) {
        var vm = this;
        vm.CategoriesId = 0;
        vm.postItem = {
            "PostId": 0,
            "PostName": '',
            'Slug': '',
            'Content': '',
            'CategoriesId': '',
            'CategoriesName': '',
            'CreateDate': '',
            'EditDate': ''
        };
        vm.categoriesItems = [];
        let objPost = {
            "PostId": 0,
            "PostName": '',
            'Slug': '',
            'Content': '',
            'CategoriesId': '',
            'CategoriesName': '',
            'CreateDate': '',
            'EditDate': ''
        };

        vm.refreshPostGird = function () {
            vm.categoriesGrid.refresh();
        };

        vm.getPost = function () {
            postService.readPost().then(function (data) {
                return data;
            }).catch(function () {
                return;
            });

        };

        vm.editPost = function (item) {
            objPost.PostId = item.PostId;
            objPost.PostName = item.PostName;
            objPost.CategoriesId = item.Category.CategoriesId;
            objPost.Content = item.Content;
            objPost.Slug = item.Slug;
            postService.editPost(objPost).then(function (data) {
                if (data.IsError) {
                    vm.showPopupFail(data.Message);
                } else {
                    vm.showPopupSuccess(data.Message);
                }
                vm.refreshGrid();
            }).catch(function (data) {
                vm.showPopupFail(data);
            });
        };

        vm.deletePost = function (item) {
            postService.deletePost(item).then(function (data) {
                if (data.IsError) {
                    vm.showPopupFail(data.Message);
                } else {
                    vm.showPopupSuccess(data.Message);
                }
                vm.refreshGrid();
            }).catch(function (data) {
                vm.showPopupFail(data);
            });
        };

        vm.addPost = function () {
            let post = vm.postItem;

            if (checkNullOrEmpty(post)) {
                vm.showPopupFail("data unvalid");
                return;
            }
            post.CategoriesId = parseInt(vm.CategoriesId);
            postService.addPost(post).then(function (data) {
                if (data.IsError) {
                    vm.showPopupFail(data.Message);
                } else {
                    vm.showPopupSuccess(data.Message);
                }
                vm.refreshGrid();
            }).catch(function (data) {
                vm.showPopupFail(data);
            });
        };
        vm.winAddPostOpen = function () {
            categoriesServices.readCategories().then(function (data) {
                vm.categoriesItems = data;
                vm.winAddpostvisible = true;
                vm.winAddPost.open();
            }).catch(function (data) {
                winAddPost.open();
            });

        };

        vm.refreshGrid = function () {
            $("#kgrid").data("kendoGrid").dataSource.read();
        };

        vm.mainGridOptions = {
            dataSource: {
                transport: {
                    read: {
                        url: envBaseUrl + '/searchposts',
                        type: 'POST',
                        contentType: 'application/json',
                        data: {}
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                schema: {
                    total: "Total",
                    data: function (response) {
                        response.Data.forEach(function (item) {
                            item.CreateDate = ConvertJsonDateToDate(item.CreateDate);
                            item.EditDate = ConvertJsonDateToDate(item.EditDate);
                        });
                        return response.Data;
                    },
                    model: {
                        id: "PostId",
                        fields: {
                            PostId: { type: "number", editable: false },
                            Slug: { type: "string" },
                            Content: { type: "string", editable: true, allowUnsort: false },
                            Category: { defaultValue: { CategoriesId: 20, CategoriesName: "Beverages" }, editable: true, },
                            CreateDate: { type: "string", editable: false },
                            EditDate: { type: "string", editable: false }
                        }
                    }

                },
                pageSize: 5,
                serverPaging: true,
                serverSorting: true
            },
            sortable: {
                mode: "multiple",
                allowUnsort: true,
                showIndexes: true
            },
            selectable: true,
            pageable: {
                refresh: true,
                pageSizes: [5, 10, 15]

            },
            autoSync: true,
            editable: true,
            toolbar: [
                {
                    name: "create",
                    text: "Add New Post",
                    template: '<a ng-click="vm.winAddPostOpen()" class="k-button k-button-icontext k-grid-create">Add New Post</a>'
                },
                {
                    name: "search",
                    template: '<input ng-model="vm.mainSearch" autocomplete="off" placeholder="Search filed..." title="Search..." class="k-input">'
                }
            ],
            databound: function () {
                this.expandrow(this.tbody.find("tr.k-master-row").first());
            },
            columns: [{
                field: "PostId",
                title: "Post Id",
                width: "120px"
            }, {
                field: "Slug",
                title: "Slug",
                width: "140px"
            }, {
                field: "Content",
                title: "Content",
                width: "270px",
                allowUnsort: true
            }, {
                field: "Category",
                title: "Categories Name",
                width: "150px",
                editor: function (container, options) {
                    vm.categoryDropDownEditor(container, options);
                },
                template: "#=Category.CategoriesName#",
                allowUnsort: true
            }, {
                field: "CreateDate",
                width: "120px"

            }, {
                field: "EditDate",
                width: "120px"
            },
            {
                command: [
                    { name: "edit", template: '<a ng-click="vm.editPost(this.dataItem)" class="k-button k-button-icontext k-grid-edit" >Edit</a>' },
                    { name: "delete", template: '<a ng-click="vm.deletePost(this.dataItem)" class="k-button k-button-icontext k-grid-delete">Delete</a>' }
                ]
            }
            ]
        };

        vm.categoryDropDownEditor = function (container, options) {
            $('<input required name="' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    autoBind: false,
                    dataTextField: "CategoriesName",
                    dataValueField: "CategoriesId",
                    dataSource: {
                        transport: {
                            read: {
                                url: envBaseUrl + '/getcategories',
                                type: 'GET',
                                dataType: 'json',
                                data: {},
                            },
                            parameterMap: function (options, type) {
                                console.log(options);
                                return JSON.stringify(options);
                            }
                        }
                    }
                });
        };
        //show notify
        vm.notifySuccessOptions = {
            templates: [{
                type: "success",
                template: $("#notificationSuccess").html()
            }]
        };

        vm.notifyFailOptions = {
            templates: [{
                type: "error",
                template: $("#notificationFail").html()
            }]
        };

        vm.showPopupSuccess = function (message) {
            vm.notifySuccess.show({
                kValue: message
            }, "success");
        };

        vm.showPopupFail = function (message) {
            vm.notifyFail.show({
                kValue: message
            }, "error");
        };

    }

})();