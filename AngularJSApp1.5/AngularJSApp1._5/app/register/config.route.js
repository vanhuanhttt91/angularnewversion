﻿(function () {
    'use strict';

    angular.module('app.register')
        .run(appRun);
    function appRun(routehelper) {
        routehelper.configureRoutes(getRoutes());
    }

    function getRoutes() {
        return [
            {
                url: '/register',
                config: {
                    templateUrl: '/app/register/register.html',
                    controller: 'registerCtrl',
                    controllerAs: 'vm',
                    title: 'Register'
                }
            }];
    }
})();