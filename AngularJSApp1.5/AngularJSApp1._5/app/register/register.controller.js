﻿(function () {
    "use strict";
    angular
        .module("app.register", ["kendo.directives"])
        .controller("registerCtrl", registerController);

    function registerController(registerService) {

        var vm = this;
        vm.registerModel = {
            userName: '',
            PassWordHash: '',
            Email: ''
        };

        vm.register = function () {
            //if (vm.validate(user)) {
            //    return;
            //}
            registerService.registerUser(vm.registerModel).then(function (data) {
                if (data.IsError) {
                    vm.showPopupFail(data.Message);
                } else {
                    vm.showPopupSuccess(data.Message);
                }
            }).catch(function (data) {
                vm.showPopupFail(data);
            });
        };

        //show notify
        vm.notifySuccessOptions = {
            templates: [{
                type: "success",
                template: $("#notificationSuccess").html()
            }]
        };

        vm.notifyFailOptions = {
            templates: [{
                type: "error",
                template: $("#notificationFail").html()
            }]
        };

        vm.showPopupSuccess = function (message) {
            vm.notifySuccess.show({
                kValue: message
            }, "success");
        };

        vm.showPopupFail = function (message) {
            vm.notifyFail.show({
                kValue: message
            }, "error");
        };
    }

})();