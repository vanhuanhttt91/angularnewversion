﻿
(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('categoriesServices', categoriesServices);

    /* @ngInject*/
    function categoriesServices($http, exception, logger) {

        var token = localStorage.getItem(userLoginKey);
        
        return {
            addCategories: function (item) {

                if (item === undefined || item === null) return;

                return $http({
                    method: 'POST',
                    params: item,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    url: envBaseUrl + '/addCategories'
                }).then(getReponseComplete).catch(getReponseFail);

                function getReponseComplete(response) {
                    return response.data;
                }

                function getReponseFail(message) {
                    exception.catcher('XHR Failed for addCategories')(message);
                    return message;
                }
            },
            deleteCategories: function (item) {
                var request = {
                    "CategoriesId": item.CategoriesId
                };
                return $http({
                    method: 'POST',
                    params: request,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    url: envBaseUrl + '/deleteCategories'
                }).then(getReponseComplete).catch(getReponseFail);

                function getReponseComplete(response) {
                    return response.data;
                }

                function getReponseFail(message) {
                    exception.catcher('XHR Failed for deleteCategories')(message);
                    return message;
                }
            },
            editCategories: function (item) {
                if (item === undefined || item === null) {
                    return;
                }
                return $http({
                    method: 'POST',
                    params: item,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    url: envBaseUrl + '/editCategories'
                }).then(getReponseComplete).catch(getReponseFail);

                function getReponseComplete(response) {
                    return response.data;
                }

                function getReponseFail(message) {
                    exception.catcher('XHR Failed for editCategories')(message);
                    return message;
                }
            },
            readCategories: function () {
                 
                return $http({
                    method: 'GET',
                    params: {},
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded', Authorization: "Bearer " + token },
                    url: envBaseUrl + '/getcategories'
                }).then(getReponseComplete).catch(function (message) {
                    exception.catcher('XHR Failed for readCategories')(message);
                    logger.info('XHR Failed for readCategories.' + message);
                });

                function getReponseComplete(response) {
                    return response.data;
                }
            }
        };
    }

})();
