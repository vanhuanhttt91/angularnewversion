﻿(function () {
    'use strict';
    angular
        .module('app.services')
        .factory('loginService', loginService);

    function loginService($http, exception, logger, $location) {
        debugger
        return {
            login: function (item) {
                if (item === undefined || item === null) return;

                return $http({
                    method: 'POST',
                    data: item,
                    headers: { 'Content-Type': 'application/json' },
                    url: envBaseUrlAuthen + '/auth/credentials'
                }).then(getReponseComplete).catch(getReponseFail);

                function getReponseComplete(response) {
                    $http.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.BearerToken;
                    localStorage.setItem(userLoginKey, JSON.stringify(response.data));
                    //authenticationService.SetCredentials($scope.username, $scope.password);
                    $location.path("/");
                    return {
                        Data: response.data,
                        IsError: false,
                        Message: "Login success!"
                    };
                }

                function getReponseFail(response) {
                    exception.catcher('XHR Failed for login')(response);
                    localStorage.removeItem(userLoginKey);
                    return {
                        Message: response.data.ResponseStatus.Message,
                        IsError: true
                    };
                }
            }
        };
    }
})();