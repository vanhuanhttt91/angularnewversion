﻿(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('postService', postService);

    function postService($http, exception, logger) {
        return {
            addPost: function (item) {
                if (item === undefined || item === null) return;

                return $http({
                    method: 'POST',
                    data: JSON.stringify(item),
                    headers: { 'Content-Type': 'application/json' },
                    url: envBaseUrl + '/addPost'
                }).then(getReponseComplete).catch(getReponseFail);

                function getReponseComplete(response) {
                    return response.data;
                }

                function getReponseFail(message) {
                    exception.catcher('XHR Failed for addPost')(message);
                    return message;
                }
            },
            deletePost: function (item) {
                var request = {
                    "PostId": item.PostId
                };
                return $http({
                    method: 'POST',
                    params: JSON.stringify(item),
                    headers: { 'Content-Type': 'application/json' },
                    url: envBaseUrl + '/deletePost'
                }).then(getReponseComplete).catch(getReponseFail);

                function getReponseComplete(response) {
                    return response.data;
                }

                function getReponseFail(message) {
                    exception.catcher('XHR Failed for deletePost')(message);
                    return message;
                }
            },
            editPost: function (item) {
                if (item === undefined || item === null) {
                    return;
                }
                return $http({
                    method: 'POST',
                    params: JSON.stringify(item),
                    headers: { 'Content-Type': 'application/json' },
                    url: envBaseUrl + '/editPost'
                }).then(getReponseComplete).catch(getReponseFail);

                function getReponseComplete(response) {
                    return response.data;
                }

                function getReponseFail(message) {
                    exception.catcher('XHR Failed for editPost')(message);
                    return message;
                }
            },
            readPost: function () {
                return $http({
                    method: 'GET',
                    params: {},
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    url: envBaseUrl + '/posts'
                }).then(getReponseComplete).catch(getReponseFail);

                function getReponseComplete(response) {
                    return response.data;
                }

                function getReponseFail(message) {
                    exception.catcher('XHR Failed for readPost')(message);
                    return message;
                }
            }
        };
    }

})();
