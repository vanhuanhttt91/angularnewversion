﻿(function () {
    'use strict';
    angular
        .module('app.services')
        .factory('registerService', registerService);

    function registerService($http, exception, logger) {
        return {
            registerUser: function (user) {
                if (user === undefined || user === null) return;

                return $http({
                    method: 'POST',
                    data: user,
                    headers: { 'Content-Type': 'application/json' },
                    url: envBaseUrl + '/register'
                }).then(getReponseComplete).catch(getReponseFail);

                function getReponseComplete(response) {
                    return response.data;
                }

                function getReponseFail(message) {
                    exception.catcher('XHR Failed for registerUser')(message);
                    return message;
                }
            }
        };
    }
})();