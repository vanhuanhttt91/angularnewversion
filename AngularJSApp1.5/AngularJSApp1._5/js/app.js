﻿/* global angular */
(function () {
    "use strict";
    // Declare app level module which depends on filters, and services
    angular.module('app', [
        'ngRoute',
        'app.navigation',
        'app.services',
        'app.categories',
        'app.post',
        'app.login',
        'app.register'
    ]).run(function ($rootScope, $location) {

        $rootScope.$on("$routeChangeStart", function(event, next, current) {
            var urlNext = next.originalPath;            
            var token = localStorage.getItem(userLoginKey) !== null ? JSON.parse(localStorage.getItem(userLoginKey)).BearerToken : null;
            if (token === null) {
                if (urlNext === '/register') {
                    return;
                };
                $location.path("/login");
            } else if (urlNext === '/register') {
                var urlCurrent = current.originalPath;
                $location.path(urlCurrent);
            }
        });
    });
   
})();