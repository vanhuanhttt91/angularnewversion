﻿/* global angular */
(function () {
    "use strict";

    var app = angular.module('addCategories.controllers', []);

    app.service("categoriesServices", categoriesServices);
    categoriesServices.$inject = ['$http'];

    app.controller('addCategoriesCtrl', ['$scope', '$http', 'categoriesServices',
        function ($scope, $http, categoriesServices) {


            $scope.getCategories = function () {
                $http({
                    method: 'GET',
                    headers: {
                        'Content-Type': "application/json"
                    },
                    url: envBaseUrl + '/categories'
                }).then(function (response) {

                    $scope.helloResult = response.data.Result;
                }, function (error) {

                });

            };
             
            $scope.addCategories = function() {
                var item = {
                    "CategoriesName": $scope.categoriesName
                };
                categoriesServices.addCategories(item).then(function(data) {
                    if (data.IsError) {
                        $scope.notifyFailOptions(data.Message);
                    } else {
                        $scope.showPopupSuccess(data.Message);
                    }
                }).catch(function(data) {
                    $scope.notifyFailOptions(data.Message);
                });
            };

            //show notify
            $scope.notifySuccessOptions = {
                templates: [{
                    type: "success",
                    template: $("#notificationSuccess").html()
                }]
            };

            $scope.notifyFailOptions = {
                templates: [{
                    type: "error",
                    template: $("#notificationFail").html()
                }]
            };

            $scope.showPopupSuccess = function (message) {
                $scope.notifySuccess.show({
                    kValue: message
                }, "success");
            };

            $scope.showPopupFail = function (message) {
                $scope.notifyFail.show({
                    kValue: message
                }, "error");
            };
        }
    ]);


})();

