﻿/* global angular */
(function () {
    "use strict";

    var app = angular.module('categories.controllers', []);

    app.service("categoriesServices", categoriesServices);
    categoriesServices.$inject = ['$http'];

    app.controller('categoriesCtrl', categoriesControllers);
    categoriesControllers.$inject = ['$scope', '$http', 'categoriesServices'];

 
    function categoriesControllers($scope, $http, categoriesServices) {

        $scope.editCategories = function(item) {
            var request = {
                "CategoriesId": item.CategoriesId,
                "CategoriesName": item.CategoriesName
            };
            categoriesServices.editCategories(request).then(function(data) {
                if (data.IsError) {
                    $scope.notifyFailOptions(data.Message);
                } else {
                    $scope.showPopupSuccess(data.Message);
                }
            }).catch(function(data) {
                $scope.showPopupSuccess(data.Message);
            });
        };

        $scope.deleteCategories = function(item) {
            categoriesServices.deleteCategories(item).then(function(data) {
                if (data.IsError) {
                    $scope.notifyFailOptions(data.Message);
                } else {
                    $scope.showPopupSuccess(data.Message);
                }
            }).catch(function(data) {
                $scope.showPopupSuccess(data.Message);
            });
        };

        //    //defined main grid
        $scope.mainGridOptions = {
            dataSource: {
                transport: {
                    read: function(options) {
                        $http.jsonp(envBaseUrl + '/categories')
                            .then(function success(response) {
                                for (var i = 0; i < response.data.length; i++) {
                                    response.data[i].CreateDate = ConvertJsonDateToDate(response.data[i].CreateDate);
                                    response.data[i].EditDate = ConvertJsonDateToDate(response.data[i].EditDate);
                                }
                                options.success(response.data);
                            }, function error(error) {
                                options.error(error);

                            });
                    }
                },
                pageSize: 5,
                serverPaging: false,
                serverSorting: false
            },
            selectable: true,
            sortable: true,
            pageable: true,
            editable: true,
            toolbar: [
                {
                    name: "create",
                    text: "Add New Categories",
                    template: '<a ng-click="winAddCategories.open()" class="k-button k-button-icontext k-grid-create" href="\\#">Add New Categories</a>'
                }
            ],
            databound: function() {
                this.expandrow(this.tbody.find("tr.k-master-row").first());
            },
            columns: [{
                field: "CategoriesId",
                title: "Categories Id",
                width: "120px",
                hidden: "true"
            }, {
                field: "CategoriesName",
                title: "Categories Name",
                width: "120px"
            }, {
                field: "CreateDate",
                width: "120px",
                disable: "true"
            }, {
                field: "EditDate",
                width: "120px"
            },
            {
                command: [
                    { name: "edit", template: '<a ng-click="editCategories(this.dataItem)" class="k-button k-button-icontext k-grid-edit" href="\\#">Edit</a>' },
                    { name: "delete", template: '<a ng-click="deleteCategories(this.dataItem)" class="k-button k-button-icontext k-grid-delete" href="\\#">Delete</a>' }
                ]
            }
            ]
        };

        //show notify
        $scope.notifySuccessOptions = {
            templates: [{
                type: "success",
                template: $("#notificationSuccess").html()
            }]
        };

        $scope.notifyFailOptions = {
            templates: [{
                type: "error",
                template: $("#notificationFail").html()
            }]
        };

        $scope.showPopupSuccess = function(message) {
            $scope.notifySuccess.show({
                kValue: message
            }, "success");
        };

        $scope.showPopupFail = function(message) {
            $scope.notifyFail.show({
                kValue: message
            }, "error");
        };
    }
})();