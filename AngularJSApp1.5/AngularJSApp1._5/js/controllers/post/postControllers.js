﻿/* global angular */
(function () {
    "use strict";

    var app = angular.module('post.controllers', []);

    app.service('categoriesServices', categoriesServices);
    app.service('postServices', postServices);
    categoriesServices.$inject = ['$http'];
    postServices.$inject = ['$http'];

    app.controller('postCtrl', postControllers);
    postControllers.$inject = ['$scope', '$http', '$rootScope', 'categoriesServices', 'postServices'];

    app.controller('addPostCtrl', postAddControllers);
    postAddControllers.$inject = ['$scope', '$http', '$rootScope'];

    function postControllers($scope, $http, $rootScope, categoriesServices, postServices) {

        $scope.refreshPostGird = function() {
            $scope.categoriesGrid.refresh();
        };

        $scope.getPost = function() {
            $http({
                method: 'GET',
                headers: {
                    'Content-Type': "application/json"
                },
                url: envBaseUrl + '/categories'
            }).then(function(response) {

                $scope.helloResult = response.data.Result;
            }, function(error) {

            });

        };

        $scope.editPost = function(item) {
            var request = {
                "PostId": item.PostId,
                "PostName": item.PostName
            };
            $http({
                method: 'POST',
                data: request,
                params: request,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: envBaseUrl + '/editcategories'
            }).then(function(response) {
                $scope.showPopup(response.data.Message);
            }, function(error) {

            });
        };

        $scope.deletePost = function(item) {
            var request = {
                "PostId": item.PostId
            };
            $http({
                method: 'POST',
                params: request,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: envBaseUrl + '/deletecategories'
            }).then(function(response) {
                $scope.showPopup(response.data.Message);
            }, function(error) {

            });
        };

        $scope.winAddPostOpen = function() {
            categoriesServices.readCategories().then(function(data) {
                $rootScope.$broadcast("loadCategories", data);
                $scope.winAddPost.open();
            }).catch(function(data) {
                $scope.winAddPost.open();
            });

        };

        //defined main grid
        $scope.mainGridOptions = {
            dataSource: {
                transport: {
                    read: function(options) {
                        postServices.readPost().then(function(data) {
                            data.forEach(function(item) {
                                item.CreateDate = ConvertJsonDateToDate(item.CreateDate);
                                item.EditDate = ConvertJsonDateToDate(item.EditDate);
                            });
                            options.success(data);
                        }).catch(function(data) {
                            options.error();
                        });

                    }
                },
                pageSize: 5,
                serverPaging: false,
                serverSorting: false
            },
            selectable: true,
            sortable: true,
            pageable: true,
            editable: true,
            toolbar: [
                {
                    name: "create",
                    text: "Add New Post",
                    template: '<a ng-click="winAddPostOpen()" class="k-button k-button-icontext k-grid-create" href="\\#">Add New Post</a>'
                }
            ],
            databound: function() {
                this.expandrow(this.tbody.find("tr.k-master-row").first());
            },
            columns: [{
                field: "PostId",
                title: "Post Id",
                width: "120px",
                hidden: "true"
            }, {
                field: "Slug",
                title: "Slug",
                width: "120px"
            }, {
                field: "Content",
                title: "Content",
                width: "120px"
            }, {
                field: "Slug",
                title: "Slug",
                width: "120px"
            }, {
                field: "CategoryId",
                title: "CategoryId",
                width: "120px"
            }, {
                field: "CreateDate",
                width: "120px",

            }, {
                field: "EditDate",
                width: "120px"
            },
            {
                command: [
                    { name: "edit", template: '<a ng-click="editPost(this.dataItem)" class="k-button k-button-icontext k-grid-edit" href="\\#">Edit</a>' },
                    { name: "delete", template: '<a ng-click="deletePost(this.dataItem)" class="k-button k-button-icontext k-grid-delete" href="\\#">Delete</a>' }
                ]
            }
            ]
        };

        //show notify
        $scope.notifySuccessOptions = {
            templates: [{
                type: "ngTemplate",
                template: $("#notificationSuccess").html()
            }]
        };

        $scope.showPopup = function(message) {
            var date = new Date();
            date = kendo.toString(date, "HH:MM:ss.") + kendo.toString(date.getMilliseconds(), "000");

            $scope.notifySuccess.show({
                kValue: message
            }, "ngTemplate");
        };

    }

    function postAddControllers($scope, $http, $rootScope) {

        $rootScope.$on("loadCategories", function(evt, data) {
            $scope.CategoriesDataSource = data;
        });

        $scope.getPost = function() {
            $http({
                method: 'GET',
                headers: {
                    'Content-Type': "application/json"
                },
                url: envBaseUrl + '/categories'
            }).then(function(response) {

                $scope.helloResult = response.data.Result;
            }, function(error) {

            });

        };

        $scope.addPost = function() {
            var request = {
                "PostName": $scope.categoriesName
            };
            $http({
                method: 'POST',
                params: request,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: envBaseUrl + '/addcategories'
            }).then(function(response) {
                $scope.showPopup(response.data.Message);
                $scope.categoriesName = "";
                $("#categoriesGrid").data("kendoGrid").dataSource.read();
            }, function(error) {
                $scope.categoriesName = "";
            });
        };



        //show notify
        $scope.notifySuccessOptions = {
            templates: [{
                type: "ngTemplate",
                template: $("#notificationSuccess").html()
            }]
        };

        $scope.showPopup = function(message) {
            var date = new Date();
            date = kendo.toString(date, "HH:MM:ss.") + kendo.toString(date.getMilliseconds(), "000");

            $scope.notifySuccess.show({
                kValue: message
            }, "ngTemplate");
        };
    }
})();

