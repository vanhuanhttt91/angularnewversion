﻿
function categoriesServices($http) {
    return {
        addCategories: function (item) {

            if (item === undefined || item === null) return;

            return $http({
                method: 'POST',
                params: item,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: envBaseUrl + '/addCategories'
            }).then(getReponseComplete).catch(getReponseFail);

            function getReponseComplete(response) {
                return response.data;
            }

            function getReponseFail(response) {
                logger.error('XHR Failed for getAvengers.' + response.data);
                return response.data;
            }
        },
        deleteCategories: function (item) {
            var request = {
                "CategoriesId": item.CategoriesId
            };
            return $http({
                method: 'POST',
                params: request,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: envBaseUrl + '/deleteCategories'
            }).then(getReponseComplete).catch(getReponseFail);

            function getReponseComplete(response) {
                return response.data;
            }

            function getReponseFail(response) {
                logger.error('XHR Failed for getAvengers.' + response.data);
                return response.data;
            }
        },
        editCategories: function (item) {
            if (item === undefined || item === null) {
                return;
            }
            return $http({
                method: 'POST',
                params: item,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: envBaseUrl + '/editCategories'
            }).then(getReponseComplete).catch(getReponseFail);

            function getReponseComplete(response) {
                return response.data;
            }

            function getReponseFail(response) {
                logger.error('XHR Failed for getAvengers.' + response.data);
                return response.data;
            }
        },
        readCategories: function () {
            return $http({
                method: 'GET',
                params: {},
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: envBaseUrl + '/categories'
            }).then(getReponseComplete).catch(getReponseFail);

            function getReponseComplete(response) {
                return response.data;
            }

            function getReponseFail(response) {
                logger.error('XHR Failed for getAvengers.' + response.data);
                return response.data;
            }
        }
    };
}

